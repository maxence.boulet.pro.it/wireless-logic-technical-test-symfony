<?php

namespace App\Command;

use stdClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ScrapDataCommand extends Command
{
    public $url = 'https://wltest.dns-systems.net/';    //it's the url of the website

    // configure the name (the part after "bin/console"), the description (), and the help of the command
    protected function configure(): void
    {
        $this
            ->setName('app:scrap-data')
            ->setDescription('this command will return a JSON of data in ' . $this->url)
            ->setHelp('By default this command return a JSON with ‘option title, ‘description’, ‘price’ and ‘discount’ keys corresponding to items in the table of this website : ' . $this->url . ' . There is no argument for this command');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $html = file_get_contents($this->url);  //recuperate the html where we can find the data wa need
        $crawler = new Crawler($html);          //Crawler initialisation, it's a set of DomElement objects
        $crawler = $crawler->filter('div.package-features');    //we select all div with a class "package-features"

        $packageArray = $this->crawlerToArray($crawler);
        $packageArray = $this->sortPackageArray($packageArray);

        $json = json_encode($packageArray);
        $output->writeln($json);

        //if the array can't be transformed into a json the command will return an error
        if ($json === false) {
            return Command::FAILURE;
        } else {
            return Command::SUCCESS;
        }
    }

    //return an array for each package with 4 values "option_title","description","price",and "discount"
    function crawlerToArray($crawler) : array
    {
        return $crawler->each(function (Crawler $node) {
            $package = new stdClass();
            $package->option_title = $node->filter('div.package-name')->text("null");
            $package->description = $node->filter('div.package-description')->text("null");
            $package->price = $node->filter('div.package-price > span')->text("null");
            $package->discount =  $node->filter('div.package-price > p')->text("null");

            //we can't filter to have only the payment type, so we need to find in the text if a monthly price or a yearly price
            $price_full_info = $node->filter('div.package-price')->text("null");
            if(strpos($price_full_info,'Per Month') ){
                $package->price_type = "Per Month";
            }elseif(strpos($price_full_info,'Per Year') ){
                $package->price_type = "Per Year";
            }else{
                $package->price_type = "null";
            }

            return $package;
        });
    }

    //sort a PackageArray by annual price with the higher price in first position
    function sortPackageArray($array) : array
    {
        usort($array, function($item1, $item2): int
        {
            $price1 =  (float) preg_replace("/[^0-9.]/", "", $item1->price);    //select the number in the price string
            $price2 =  (float) preg_replace("/[^0-9.]/", "", $item2->price);    //select the number in the price string

            if($item1->price_type === "Per Month"){    //if the price it's by month we need to multiply by 12 for know the annual price
                $price1 = $price1*12;
            }
            if($item2->price_type === "Per Month"){    //if the price it's by month we need to multiply by 12 for know the annual price
                $price2 = $price2*12;
            }

            if ($price1 == $price2) {
                return 0;
            }
            return ($price1 > $price2) ? -1 : 1;
        });
        return $array;
    }

}