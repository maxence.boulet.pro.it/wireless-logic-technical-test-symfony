<?php


use App\Command\ScrapDataCommand;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DomCrawler\Crawler;

class ScrapDataCommandTest extends KernelTestCase
{
    /**
     * @test
     * test if the url in scrapDataCommand is the correct url
     */
    public function urlTest()
    {
        self::bootKernel();
        $container = static::getContainer();
        $scrapData = $container->get(ScrapDataCommand::class);
        $this->assertEquals('https://wltest.dns-systems.net/', $scrapData->url);
    }

    /**
     * @test
     * test if the website contain the class "package-features", "package-name", "package-description", "package-price"
     */
    public function webSiteContentTest()
    {
        self::bootKernel();
        $container = static::getContainer();
        $scrapData = $container->get(ScrapDataCommand::class);
        $htmlArray = explode('"',file_get_contents($scrapData->url));   //transform the html into an array
        $this->assertContains("package-features",$htmlArray);
        $this->assertContains("package-name",$htmlArray);
        $this->assertContains("package-description",$htmlArray);
        $this->assertContains("package-price",$htmlArray);
    }

    /**
     * @test
     * test if the sortPackage return an array correctly sorted
     */
    public function sortPackageArrayTest(){
        {
            self::bootKernel();
            $container = static::getContainer();
            $scrapData = $container->get(ScrapDataCommand::class);

            $package1 = new stdClass();
            $package1->price = "£27.99";
            $package1->price_type = "Per Year";
            $array[]=$package1;

            $package2 =  new stdClass();
            $package2->price = "£5.99";
            $package2->price_type = "Per Month";
            $array[] = $package2;

            $array = $scrapData->sortPackageArray($array);
            $this->assertEquals("£5.99", $array[0]->price);
            $this->assertEquals("Per Month", $array[0]->price_type);
            $this->assertEquals("£27.99", $array[1]->price);
            $this->assertEquals("Per Year", $array[1]->price_type);
        }
    }
}