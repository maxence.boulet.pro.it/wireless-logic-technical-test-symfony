# Wireless Logic - Technical Test - Symfony

## Description
This app contains a command who take information on the website https://wltest.dns-systems.net/ and return them into a json format.

## Prerequisites

- PHP 7.2
- Symfony 5.4

## Getting started

Open a terminal
```
git clone https://gitlab.com/maxence.boulet.pro.it/wireless-logic-technical-test-symfony.git
cd wireless-logic-technical-test-symfony
composer install
```
now you have all the files.

## Launch the command

```
php bin/console app:scrap-data
```
## Launch the Test
```
php bin/phpunit
```

***
this project was done by : Maxence Boulet